package com.bbva.moneyexchange.api.config.security.serviceimpl;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.service.AuthorityService;
import com.bbva.moneyexchange.api.service.RoleService;
import com.bbva.moneyexchange.api.service.UserService;
import com.bbva.moneyexchange.api.util.allocator.AuthorityAllocator;
import com.bbva.moneyexchange.api.util.allocator.RoleAllocator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("OwnUserDetailServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserService userService;
    private AuthorityService authorityService;
    private RoleService roleService;

    public UserDetailsServiceImpl(UserService userService, AuthorityService authorityService, RoleService roleService) {
        this.userService = userService;
        this.authorityService = authorityService;
        this.roleService = roleService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optional = userService.findBy(username);
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException(String.format("User not found with username: %s", username));
        }
        User user = optional.get();
        final List<Role> roles = roleService.findAllBy(user);
        final Collection<GrantedAuthority> grantedRoles = RoleAllocator.toGrantedRoles(roles);
        final List<Authority> authorities = authorityService.findAllBy(user);
        final Collection<GrantedAuthority> grantedAuthorities = AuthorityAllocator.toGrantedAuthorities(authorities);
        final List<GrantedAuthority> grantedUser = Stream.concat(grantedRoles.stream(), grantedAuthorities.stream()).collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedUser);
    }
}
