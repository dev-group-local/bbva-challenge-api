package com.bbva.moneyexchange.api.config.security.filter;

import com.bbva.moneyexchange.api.config.security.constant.SecurityConstant;
import com.bbva.moneyexchange.api.config.security.util.JWTTokenUtil;
import com.bbva.moneyexchange.api.entity.Person;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.service.UserService;
import com.bbva.moneyexchange.api.service.UserroleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private JWTTokenUtil jwtTokenUtil;
    private UserService userService;
    private UserroleService userroleService;
    private User userEntity;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
                                   JWTTokenUtil jwtTokenUtil,
                                   UserService userService,
                                   UserroleService userroleService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
        this.userroleService = userroleService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
        try {
            userEntity = new ObjectMapper().readValue(req.getInputStream(), User.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                userEntity.getUsername(),
                userEntity.getPassword(),
                new ArrayList<>()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication auth) {
        String subject = ((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername();
        String token = jwtTokenUtil.buildBy(subject);
        User user = userService.getBy(subject);
        Person person = user.getPerson();
        Integer personId = person.getId();
        response.addHeader("PersonId", String.valueOf(personId));
        response.addHeader(SecurityConstant.HEADER_STRING, SecurityConstant.TOKEN_PREFIX + token);
    }
}
