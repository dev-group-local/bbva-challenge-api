package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.pojo.SearchModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ExchangeService extends GenericService<Exchange> {

    Exchange register(User user, Exchange exchange);

    Page<Exchange> findPagedBy(SearchModel searchModel, Pageable pageable);
}
