package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Person;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;

import java.util.List;

public interface RoleService extends GenericService<Role> {

    List<Role> findAllBy(int roleId);

    List<Role> findAllBy(User user);

    List<Role> findAllBy(Person person);

    boolean has(Person person, String roleCode);
}
