package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userrole;

import java.util.List;
import java.util.Optional;

public interface UserroleService extends GenericService<Userrole> {

    Userrole save(Userrole userrole);

    List<Userrole> findAllBy(User user);

    Optional<Userrole> findBy(User user, Role role);
}
