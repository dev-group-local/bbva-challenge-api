package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.Exchangetype;

import java.util.Optional;

public interface ExchangetypeService extends GenericService<Exchangetype> {

    Exchange convert(String originCurrency, String originAmount, String targetcurrency);

    Optional<Exchangetype> findBy(String origin, String target);

    Exchangetype getBy(String origin, String target);
}
