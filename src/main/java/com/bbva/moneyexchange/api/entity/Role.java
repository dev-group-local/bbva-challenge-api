package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Role {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "code", nullable = true, length = 64)
    private String code;
    @Basic
    @Column(name = "abbreviation", nullable = true, length = 32)
    private String abbreviation;
    @Basic
    @Column(name = "nameView", nullable = true, length = 32)
    private String nameView;
    @Basic
    @Column(name = "description", nullable = true, length = 128)
    private String description;
    @Basic
    @Column(name = "position", nullable = true)
    private Integer position;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;
    @Basic
    @Column(name = "active", nullable = true)
    private Boolean active;
    @Basic
    @Column(name = "deleted", nullable = true)
    private Boolean deleted;
    @Basic
    @Column(name = "observation", nullable = true, length = 64)
    private String observation;
    @OneToMany(mappedBy = "role")
    private Collection<Roleauthority> roleauthoritiesById;
    @OneToMany(mappedBy = "role")
    private Collection<Userrole> userrolesById;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getNameView() {
        return nameView;
    }

    public void setNameView(String nameView) {
        this.nameView = nameView;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(id, role.id) && Objects.equals(code, role.code) && Objects.equals(abbreviation, role.abbreviation) && Objects.equals(nameView, role.nameView) && Objects.equals(description, role.description) && Objects.equals(position, role.position) && Objects.equals(createdAt, role.createdAt) && Objects.equals(active, role.active) && Objects.equals(deleted, role.deleted) && Objects.equals(observation, role.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, abbreviation, nameView, description, position, createdAt, active, deleted, observation);
    }

    public Collection<Roleauthority> getRoleauthoritiesById() {
        return roleauthoritiesById;
    }

    public void setRoleauthoritiesById(Collection<Roleauthority> roleauthoritiesById) {
        this.roleauthoritiesById = roleauthoritiesById;
    }

    public Collection<Userrole> getUserrolesById() {
        return userrolesById;
    }

    public void setUserrolesById(Collection<Userrole> userrolesById) {
        this.userrolesById = userrolesById;
    }
}
