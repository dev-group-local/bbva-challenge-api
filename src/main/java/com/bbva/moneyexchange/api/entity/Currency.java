package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Currency {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "key", nullable = true, length = 3)
    private String key;
    @Basic
    @Column(name = "description", nullable = true, length = 12)
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(id, currency.id) && Objects.equals(key, currency.key) && Objects.equals(description, currency.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, description);
    }
}
