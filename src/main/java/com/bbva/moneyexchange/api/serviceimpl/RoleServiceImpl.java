package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Person;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userrole;
import com.bbva.moneyexchange.api.repository.RoleRepository;
import com.bbva.moneyexchange.api.service.RoleService;
import com.bbva.moneyexchange.api.service.RoleauthorityService;
import com.bbva.moneyexchange.api.service.UserroleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role, RoleRepository> implements RoleService {

    @Autowired
    private UserroleService userroleService;
    @Autowired
    private RoleauthorityService roleauthorityService;

    public List<Role> findAllBy(int roleId) {
        return repository.findAllByUserId(roleId);
    }

    @Override
    public List<Role> findAllBy(User user) {
        return userroleService.findAllBy(user)
                .stream()
                .map(Userrole::getRole)
                .collect(Collectors.toList());
    }

    @Override
    public List<Role> findAllBy(Person person) {
        User user = person.getUser();
        return userroleService.findAllBy(user)
                .stream()
                .map(Userrole::getRole)
                .collect(Collectors.toList());
    }

    public boolean has(Person person, String roleCode) {
        List<Role> roles = this.findAllBy(person);
        return roles.stream().anyMatch(role -> role.getCode().equals(roleCode));
    }
}
