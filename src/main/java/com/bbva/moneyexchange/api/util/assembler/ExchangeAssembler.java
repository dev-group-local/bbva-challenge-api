package com.bbva.moneyexchange.api.util.assembler;

import com.bbva.moneyexchange.api.controller.ExchangeController;
import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.pojo.ExchangeModel;
import com.bbva.moneyexchange.api.util.some.DecimalUtil;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class ExchangeAssembler extends RepresentationModelAssemblerSupport<Exchange, ExchangeModel> {

    public ExchangeAssembler() {
        super(ExchangeController.class, ExchangeModel.class);
    }

    @Override
    public ExchangeModel toModel(Exchange exchange) {
        ExchangeModel model = instantiateModel(exchange);
        model.setId(exchange.getId());
        model.setOriginCurrency(exchange.getOriginCurrency());
        model.setOriginAmount(DecimalUtil.print(exchange.getOriginAmount(), 4));
        model.setTargetCurrency(exchange.getTargetCurrency());
        model.setTargetAmount(DecimalUtil.print(exchange.getTargetAmount(), 4));
        model.setCreatedAt(exchange.getCreatedAt());
        return model;
    }
}
