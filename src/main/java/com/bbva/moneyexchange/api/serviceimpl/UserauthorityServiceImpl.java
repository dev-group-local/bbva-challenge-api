package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userauthority;
import com.bbva.moneyexchange.api.repository.UserauthorityRepository;
import com.bbva.moneyexchange.api.service.UserauthorityService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserauthorityServiceImpl extends GenericServiceImpl<Userauthority, UserauthorityRepository> implements UserauthorityService {

    @Override
    public Userauthority save(Userauthority userauthority) {
        return repository.save(userauthority);
    }

    public List<Userauthority> findAllBy(User user) {
        return repository.findAllByUserAndActiveTrueAndDeletedFalse(user);
    }
}
