package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.Exchangetype;
import com.bbva.moneyexchange.api.repository.ExchangetypeRepository;
import com.bbva.moneyexchange.api.service.ExchangetypeService;
import com.bbva.moneyexchange.api.util.constant.Currency;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class ExchangetypeServiceImpl extends GenericServiceImpl<Exchangetype, ExchangetypeRepository> implements ExchangetypeService {

    public Exchange convert(String originCurrency, String originAmount, String targetcurrency) {
        Exchange exchange = new Exchange();
        exchange.setTargetCurrency(targetcurrency);
        if ((originCurrency.equals(Currency.PEN) && targetcurrency.equals(Currency.USD)) || (originCurrency.equals(Currency.USD) && targetcurrency.equals(Currency.PEN))) {
            Exchangetype exchangetype = this.getBy(originCurrency, targetcurrency);
            BigDecimal targetAmount = new BigDecimal(originAmount).multiply(exchangetype.getRate());
            exchange.setTargetAmount(targetAmount);
            return exchange;
        } else if ((originCurrency.equals(Currency.PEN) && targetcurrency.equals(Currency.PEN)) || (originCurrency.equals(Currency.USD) && targetcurrency.equals(Currency.USD))) {
            exchange.setTargetAmount(new BigDecimal(originAmount));
            return exchange;
        } else {
            throw new RuntimeException(String.format("Parámetros de conversión inválidos: %s, %s", originCurrency, targetcurrency));
        }
    }

    public Optional<Exchangetype> findBy(String origin, String target) {
        return repository.findFirstByOriginAndTargetAndActiveTrueOrderByIdDesc(origin, target);
    }

    public Exchangetype getBy(String origin, String target) {
        Optional<Exchangetype> optional = this.findBy(origin, target);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new RuntimeException(String.format("Tipo de cambio no definido para %s -> %s", origin, target));
    }
}
