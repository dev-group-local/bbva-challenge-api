package com.bbva.moneyexchange.api.controller;

import com.bbva.moneyexchange.api.converter.ExchangeConverter;
import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.pojo.ExchangeModel;
import com.bbva.moneyexchange.api.service.ExchangetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class ExchangetypeController {

    @Autowired
    private ExchangetypeService exchangetypeService;

    @GetMapping(value = "/type-exchanges")
    @PreAuthorize("true")
    public ResponseEntity<ExchangeModel> convert(@RequestParam String originCurrency,
                                                 @RequestParam String originAmount,
                                                 @RequestParam String targetCurrency,
                                                 HttpServletRequest request) {
        //TODO Validations
        Exchange exchange = exchangetypeService.convert(originCurrency, originAmount, targetCurrency);
        final ExchangeModel model = new ExchangeConverter().doItReverse(exchange);
        return ResponseEntity.ok().body(model);
    }
}
