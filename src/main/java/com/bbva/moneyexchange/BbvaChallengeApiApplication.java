package com.bbva.moneyexchange;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication(scanBasePackages = {
        "com.bbva.moneyexchange.api.controller",
        "com.bbva.moneyexchange.api.serviceimpl",
        "com.bbva.moneyexchange.api.config.security"}
)
@EnableJpaRepositories("com.bbva.moneyexchange.api.repository")
@EntityScan("com.bbva.moneyexchange.api.entity")
public class BbvaChallengeApiApplication implements CommandLineRunner {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(BbvaChallengeApiApplication.class, args);
    }

    @Override
    public void run(String... strings) {
    }

}
