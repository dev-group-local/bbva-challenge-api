package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.config.security.util.JWTTokenUtil;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.repository.UserRepository;
import com.bbva.moneyexchange.api.service.AuthorityService;
import com.bbva.moneyexchange.api.service.RoleService;
import com.bbva.moneyexchange.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, UserRepository> implements UserService {

    @Autowired
    private JWTTokenUtil jwtTokenUtil;
    @Autowired
    private RoleService roleService;
    @Autowired
    private AuthorityService authorityService;

    @Override
    public Optional<User> findBy(String username) {
        return repository.findByUsernameAndActiveTrueAndDeletedFalse(username);
    }

    @Override
    public User getBy(String username) {
        Optional<User> optional = findBy(username);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new RuntimeException(String.format("User not found by username: %s", username));
    }

    @Override
    public User getBy(HttpServletRequest request) {
        String username = jwtTokenUtil.getSubjectOf(request);
        return this.getBy(username);
    }
}
