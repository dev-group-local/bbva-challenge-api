package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userrole;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface UserroleRepository extends PagingAndSortingRepository<Userrole, Integer> {

    List<Userrole> findAllByUserAndActiveTrueAndDeletedFalse(User user);

    Optional<Userrole> findByUserAndRoleAndActiveTrueAndDeletedFalse(User user, Role role);
}
