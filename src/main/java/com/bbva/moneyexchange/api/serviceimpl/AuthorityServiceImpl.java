package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Person;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userauthority;
import com.bbva.moneyexchange.api.repository.AuthorityRepository;
import com.bbva.moneyexchange.api.service.AuthorityService;
import com.bbva.moneyexchange.api.service.UserauthorityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AuthorityServiceImpl extends GenericServiceImpl<Authority, AuthorityRepository> implements AuthorityService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    @Autowired
    private UserauthorityService userauthorityService;

    /**
     * Busca los permisos asociados a los roles que tenga el usuario
     *
     * @param user
     * @return
     */
    @Override
    public List<Authority> findAllRoleAssociatedBy(User user) {
        return repository.findAllRoleAssociatedBy(user.getId());
    }

    /**
     * Busca los permisos <b>extra</b> asociados al usuario
     *
     * @param user
     * @return
     */
    public List<Authority> findAllExtraBy(User user) {
        return userauthorityService.findAllBy(user)
                .stream()
                .map(Userauthority::getAuthority)
                .collect(Collectors.toList());
    }

    /**
     * Busca los permisos asignados al usuario; tanto los asociados a sus roles, así como los permisos extra
     *
     * @param user
     * @return
     */
    public List<Authority> findAllBy(User user) {
        List<Authority> roleAssociated = this.findAllRoleAssociatedBy(user);
        List<Authority> extra = this.findAllExtraBy(user);
        return Stream
                .concat(roleAssociated.stream(), extra.stream())
                .distinct()
                .collect(Collectors.toList());
    }

    public List<Authority> findAllBy(Person person) {
        User user = person.getUser();
        return findAllBy(user);
    }

    /**
     * Verifica si la persona a verificar tiene el permiso a consultar
     *
     * @param person
     * @param authorityCode
     * @return
     */
    public boolean has(Person person, String authorityCode) {
        List<Authority> authorities = this.findAllBy(person);
        return authorities.stream().anyMatch(authority -> authority.getCode().equals(authorityCode));
    }
}
