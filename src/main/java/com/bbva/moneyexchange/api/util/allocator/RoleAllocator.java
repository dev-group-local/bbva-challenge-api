package com.bbva.moneyexchange.api.util.allocator;

import com.bbva.moneyexchange.api.entity.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

public class RoleAllocator {

    private RoleAllocator() {
    }

    public static Collection<GrantedAuthority> toGrantedRoles(Collection<Role> roles) {
        return roles.stream().map(Role::getCode).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public static void ofPostMethod(Role role) {
        role.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        role.setActive(Boolean.TRUE);
        role.setDeleted(Boolean.FALSE);
    }
}
