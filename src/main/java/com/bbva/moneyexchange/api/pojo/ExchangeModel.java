package com.bbva.moneyexchange.api.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "exchanges")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExchangeModel extends RepresentationModel<ExchangeModel> {
    private Integer id;
    private String originCurrency;
    private String originAmount;
    private String targetCurrency;
    private String targetAmount;
    private Timestamp createdAt;
}
