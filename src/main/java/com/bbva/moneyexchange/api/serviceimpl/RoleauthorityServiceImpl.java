package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.Roleauthority;
import com.bbva.moneyexchange.api.repository.RoleauthorityRepository;
import com.bbva.moneyexchange.api.service.RoleauthorityService;
import com.bbva.moneyexchange.api.service.UserroleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleauthorityServiceImpl extends GenericServiceImpl<Roleauthority, RoleauthorityRepository> implements RoleauthorityService {

    @Autowired
    private UserroleService userroleService;


    @Override
    public Optional<Roleauthority> findBy(Role role, Authority authority) {
        return repository.findByRoleAndAuthorityAndActiveTrueAndDeletedFalse(role, authority);
    }

    @Override
    public Roleauthority getBy(Role role, Authority authority) {
        Optional<Roleauthority> optional = this.findBy(role, authority);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new RuntimeException(String.format("Roleauthority not found by role: %s, authority: %s", role, authority));
    }
}
