package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.Roleauthority;

import java.util.Optional;

public interface RoleauthorityService extends GenericService<Roleauthority> {

    Optional<Roleauthority> findBy(Role role, Authority authority);

    Roleauthority getBy(Role role, Authority authority);
}
