package com.bbva.moneyexchange.api.converter;

import com.bbva.moneyexchange.api.pojo.SearchModel;

public class SearchConverter {

    public SearchModel doIt(String originCurrency,
                                  String targetcurrency,
                                  String createdAt) {
        SearchModel model = new SearchModel();
        model.setOriginCurrency(originCurrency);
        model.setTargetcurrency(targetcurrency);
        model.setCreatedAt(createdAt);
        return model;
    }
}
