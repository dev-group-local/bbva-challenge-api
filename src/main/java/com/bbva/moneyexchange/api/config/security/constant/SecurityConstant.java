package com.bbva.moneyexchange.api.config.security.constant;

public class SecurityConstant {

    public static final String SECRET = "XXM4y0rLyutfDtuilc9eX!@16-&%-2087*El Perú necesita de tod@$; t0d0$ ponem0$ el h0mbr0";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
