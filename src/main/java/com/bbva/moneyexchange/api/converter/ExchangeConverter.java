package com.bbva.moneyexchange.api.converter;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.pojo.ExchangeModel;
import com.bbva.moneyexchange.api.util.some.DecimalUtil;

import java.math.BigDecimal;

public class ExchangeConverter {

    public Exchange doIt(ExchangeModel model) {
        Exchange exchange = new Exchange();
        exchange.setOriginCurrency(model.getOriginCurrency());
        exchange.setOriginAmount(new BigDecimal(model.getOriginAmount()));
        exchange.setTargetCurrency(model.getTargetCurrency());
        exchange.setTargetAmount(new BigDecimal(model.getTargetAmount()));
        return exchange;
    }

    public ExchangeModel doItReverse(Exchange exchange) {
        ExchangeModel model = new ExchangeModel();
        model.setId(exchange.getId());
        model.setTargetCurrency(exchange.getTargetCurrency());
        model.setTargetAmount(DecimalUtil.print(exchange.getTargetAmount(), 4));
        return model;
    }
}
