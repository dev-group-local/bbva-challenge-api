package com.bbva.moneyexchange.api.util.some;

import io.jsonwebtoken.lang.Assert;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DecimalUtil {

    private DecimalUtil() {
    }

    public static BigDecimal format(BigDecimal number, int decimals) {
        Assert.notNull(number, "Number can't be null");
        StringBuilder sb = new StringBuilder(decimals + 2);
        sb.append("#.");
        for (int index = 0; index < decimals; index++) {
            sb.append("0");
        }
        final DecimalFormat df = new DecimalFormat(sb.toString());
        final DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(sym);
        final String result = df.format(number);
        return new BigDecimal(result);
    }

    /**
     * Redondea a dos decimales y convierte en cadena el número enviado
     *
     * @param number
     * @return
     */
    public static String print(BigDecimal number) {
        if (number == null) return "";
        BigDecimal bigDecimal = format(number, 2);
        return String.valueOf(bigDecimal);
    }

    public static String print(BigDecimal number, int decimals) {
        BigDecimal bigDecimal = format(number, decimals);
        return String.valueOf(bigDecimal);
    }
}
