package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userrole;
import com.bbva.moneyexchange.api.repository.UserroleRepository;
import com.bbva.moneyexchange.api.service.UserroleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserroleServiceImpl extends GenericServiceImpl<Userrole, UserroleRepository> implements UserroleService {

    @Override
    public Userrole save(Userrole userrole) {
        return repository.save(userrole);
    }

    public List<Userrole> findAllBy(User user) {
        return repository.findAllByUserAndActiveTrueAndDeletedFalse(user);
    }

    @Override
    public Optional<Userrole> findBy(User user, Role role) {
        return repository.findByUserAndRoleAndActiveTrueAndDeletedFalse(user, role);
    }
}
