package com.bbva.moneyexchange.api.controller;

import com.bbva.moneyexchange.api.converter.ExchangeConverter;
import com.bbva.moneyexchange.api.converter.SearchConverter;
import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.pojo.ExchangeModel;
import com.bbva.moneyexchange.api.pojo.SearchModel;
import com.bbva.moneyexchange.api.service.ExchangeService;
import com.bbva.moneyexchange.api.service.UserService;
import com.bbva.moneyexchange.api.util.assembler.ExchangeAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class ExchangeController {

    @Autowired
    private ExchangeService exchangeService;
    @Autowired
    private UserService userService;

    private PagedResourcesAssembler resourcesAssembler = new PagedResourcesAssembler(null, null);

    @PostMapping(value = "/exchanges")
    @PreAuthorize("true")
    public ResponseEntity register(@RequestBody ExchangeModel model, HttpServletRequest request) {
        //TODO Validations
        User user = userService.getBy(request);
        Exchange exchange = new ExchangeConverter().doIt(model);
        exchangeService.register(user, exchange);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/exchanges")
    @PreAuthorize("true")
    public ResponseEntity<CollectionModel<ExchangeModel>> findBy(@RequestParam("originCurrency") String originCurrency,
                                                                 @RequestParam("targetCurrency") String targetCurrency,
                                                                 @RequestParam("createdAt") String createdAt,
                                                                 Pageable pageable,
                                                                 HttpServletRequest request) {
        //TODO Validations
        SearchModel model = new SearchConverter().doIt(originCurrency, targetCurrency, createdAt);
        Page<Exchange> exchanges = exchangeService.findPagedBy(model, pageable);
        ExchangeAssembler assembler = new ExchangeAssembler();
        PagedModel<ExchangeModel> pagedModel = resourcesAssembler.toModel(exchanges, assembler);
        return ResponseEntity.ok(pagedModel);
    }
}
