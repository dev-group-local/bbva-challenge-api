package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Userrole {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "userId", nullable = false)
    private Integer userId;
    @Basic
    @Column(name = "roleId", nullable = false)
    private Integer roleId;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;
    @Basic
    @Column(name = "active", nullable = true)
    private Boolean active;
    @Basic
    @Column(name = "deleted", nullable = true)
    private Boolean deleted;
    @Basic
    @Column(name = "observation", nullable = true, length = 64)
    private String observation;
    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private User user;
    @ManyToOne
    @JoinColumn(name = "roleId", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Role role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Userrole userrole = (Userrole) o;
        return Objects.equals(id, userrole.id) && Objects.equals(userId, userrole.userId) && Objects.equals(roleId, userrole.roleId) && Objects.equals(createdAt, userrole.createdAt) && Objects.equals(active, userrole.active) && Objects.equals(deleted, userrole.deleted) && Objects.equals(observation, userrole.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, roleId, createdAt, active, deleted, observation);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
