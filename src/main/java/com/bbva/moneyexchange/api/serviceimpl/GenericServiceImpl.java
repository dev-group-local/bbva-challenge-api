package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

public class GenericServiceImpl<E, R extends CrudRepository<E, Integer>> implements GenericService<E> {

    @Autowired
    protected R repository;

    @Override
    public E save(E e) {
        return repository.save(e);
    }

    @Override
    public void deleteBy(int id) {
        // Éste debería ser implementado por aquellos que van a eliminar una entidad
    }

    @Override
    public void deleteBy(E e) {
        // Éste debería ser implementado por aquellos que van a eliminar una entidad
    }

    @Override
    public E register(E e) {
        return e;
    }

    @Override
    public void edit(E e) {
        // Éste debería ser implementado por aquellos que van a editar una entidad
    }
}
