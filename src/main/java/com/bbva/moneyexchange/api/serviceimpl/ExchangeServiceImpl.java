package com.bbva.moneyexchange.api.serviceimpl;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.pojo.SearchModel;
import com.bbva.moneyexchange.api.repository.ExchangeRepository;
import com.bbva.moneyexchange.api.service.ExchangeService;
import com.bbva.moneyexchange.api.service.ExchangetypeService;
import com.bbva.moneyexchange.api.util.allocator.ExchangeAllocator;
import com.bbva.moneyexchange.api.util.spec.ExchangeSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ExchangeServiceImpl extends GenericServiceImpl<Exchange, ExchangeRepository> implements ExchangeService {

    @Autowired
    private ExchangetypeService exchangetypeService;

    public Exchange register(User user, Exchange exchange) {
        ExchangeAllocator.forRegister(user, exchange);
        return this.save(exchange);
    }

    public Page<Exchange> findPagedBy(SearchModel model, Pageable pageable) {
        ExchangeSpec personSpec = new ExchangeSpec(model);
        return repository.findAll(personSpec, pageable);
    }

}
