package com.bbva.moneyexchange.api.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.server.core.Relation;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "exchanges")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchModel {

    private String originCurrency;
    private String targetcurrency;
    private String createdAt;
}
