package com.bbva.moneyexchange.api.config;

public enum Environment {
    LOCAL,
    DEVELOPMENT,
    PRODUCTION
}
