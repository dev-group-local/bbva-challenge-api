package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userauthority;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserauthorityRepository extends PagingAndSortingRepository<Userauthority, Integer> {

    List<Userauthority> findAllByUserAndActiveTrueAndDeletedFalse(User user);
}
