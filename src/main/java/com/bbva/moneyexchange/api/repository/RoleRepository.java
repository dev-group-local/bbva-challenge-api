package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Role;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RoleRepository extends PagingAndSortingRepository<Role, Integer>, JpaSpecificationExecutor<Role> {

    @Query(value = "SELECT ur.role " +
            "FROM Userrole ur " +
            "WHERE ur.userId =:userId")
    List<Role> findAllByUserId(int userId);
}
