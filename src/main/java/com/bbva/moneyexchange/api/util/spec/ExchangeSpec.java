package com.bbva.moneyexchange.api.util.spec;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.pojo.SearchModel;
import com.bbva.moneyexchange.api.util.some.DateUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Based on https://itqna.net/questions/58634/specification-spring-data-jpa-passing-null-parameter
 */
public class ExchangeSpec implements Specification<Exchange> {

    private String originCurrency;
    private String targetCurrency;
    private String createdAt;

    List<Predicate> predicates = new ArrayList<>();

    public ExchangeSpec(SearchModel model) {
        this.originCurrency = model.getOriginCurrency();
        this.targetCurrency = model.getTargetcurrency();
        this.createdAt = model.getCreatedAt();
    }

    @Override
    public Predicate toPredicate(Root<Exchange> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (originCurrency.isEmpty() || originCurrency.equals("00")) {
            predicates.add(builder.isNotNull(root.get("originCurrency")));
        } else {
            predicates.add(builder.like(root.get("originCurrency"), "%" + originCurrency + "%"));
        }

        //TODO
        if (targetCurrency.isEmpty() || targetCurrency.equals("00")) {
            predicates.add(builder.isNotNull(root.get("targetCurrency")));
        } else {
            predicates.add(builder.like(root.get("targetCurrency"), "%" + targetCurrency + "%"));
        }

        if (!createdAt.isEmpty() && !createdAt.equals("00")) {
            String noBefore = createdAt + " 00:00:00";
            String noAfter = createdAt + " 23:59:59";
            String pattern = "dd-MM-yyyy HH:mm:ss";
            predicates.add(builder.greaterThanOrEqualTo(root.get("createdAt"), DateUtil.cast(noBefore, pattern)));
            predicates.add(builder.lessThanOrEqualTo(root.get("createdAt"), DateUtil.cast(noAfter, pattern)));
        }
        return builder.and(predicates.toArray(new Predicate[1]));
    }
}
