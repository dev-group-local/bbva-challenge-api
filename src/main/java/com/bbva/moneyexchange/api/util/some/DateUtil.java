package com.bbva.moneyexchange.api.util.some;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {

    private DateUtil() {
    }

    public static java.util.Date cast(String date, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(String.format("Parsing date: %s", date));
        }
    }

    public static Timestamp cast(java.util.Date date) {
        return new Timestamp(date.getTime());
    }
}
