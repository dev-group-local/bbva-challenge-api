package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Person {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "documenttypeId", nullable = true)
    private Integer documenttypeId;
    @Basic
    @Column(name = "documentNumber", nullable = true, length = 18)
    private String documentNumber;
    @Basic
    @Column(name = "firstName", nullable = true, length = 64)
    private String firstName;
    @Basic
    @Column(name = "lastName", nullable = true, length = 64)
    private String lastName;
    @Basic
    @Column(name = "fullName", nullable = true, length = 128)
    private String fullName;
    @Basic
    @Column(name = "email", nullable = true, length = 256)
    private String email;
    @Basic
    @Column(name = "cellphone", nullable = true, length = 32)
    private String cellphone;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;
    @Basic
    @Column(name = "active", nullable = true)
    private Boolean active;
    @Basic
    @Column(name = "deleted", nullable = true)
    private Boolean deleted;
    @Basic
    @Column(name = "observation", nullable = true, length = 64)
    private String observation;
    @OneToOne(mappedBy = "person")
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDocumenttypeId() {
        return documenttypeId;
    }

    public void setDocumenttypeId(Integer documenttypeId) {
        this.documenttypeId = documenttypeId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) && Objects.equals(documenttypeId, person.documenttypeId) && Objects.equals(documentNumber, person.documentNumber) && Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName) && Objects.equals(fullName, person.fullName) && Objects.equals(email, person.email) && Objects.equals(cellphone, person.cellphone) && Objects.equals(createdAt, person.createdAt) && Objects.equals(active, person.active) && Objects.equals(deleted, person.deleted) && Objects.equals(observation, person.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, documenttypeId, documentNumber, firstName, lastName, fullName, email, cellphone, createdAt, active, deleted, observation);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
