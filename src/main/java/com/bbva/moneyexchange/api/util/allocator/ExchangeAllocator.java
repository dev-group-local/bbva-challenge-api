package com.bbva.moneyexchange.api.util.allocator;

import com.bbva.moneyexchange.api.entity.Exchange;
import com.bbva.moneyexchange.api.entity.User;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ExchangeAllocator {

    private ExchangeAllocator() {
    }

    public static void forRegister(User user, Exchange exchange) {
        exchange.setRequestor(user.getUsername());
        ofPostMethod(exchange);
    }

    public static void ofPostMethod(Exchange exchange) {
        exchange.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
    }
}
