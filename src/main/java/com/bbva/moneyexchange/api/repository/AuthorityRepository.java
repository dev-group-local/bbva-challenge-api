package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Role;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorityRepository extends PagingAndSortingRepository<Authority, Integer>, JpaSpecificationExecutor<Authority> {

    Optional<Authority> findByCodeAndActiveTrueAndDeletedFalse(String code);

    Optional<Authority> findByIdAndDeletedFalse(int id);

    @Query(value = "SELECT ra.authority " +
            "FROM Roleauthority ra " +
            "WHERE ra.role = :role " +
            "AND ra.active = true " +
            "AND ra.deleted = false " +
            "AND ra.authority.active = true " +
            "AND ra.authority.deleted = false ")
    List<Authority> findAllBy(Role role);

    @Query(value = "SELECT DISTINCT a.* FROM userrole ur " +
            "INNER JOIN role r ON r.id = ur.roleId " +
            "INNER JOIN roleauthority ra ON ra.roleId = r.id " +
            "INNER JOIN authority a ON a.id = ra.authorityId " +
            "INNER JOIN user u ON u.id = ur.userId " +
            "WHERE ur.active = true AND ur.deleted = false " +
            "AND r.active = true AND r.deleted = false " +
            "AND ra.active = true AND ra.deleted = false " +
            "AND a.active = true AND a.deleted = false " +
            "AND u.id = :userId", nativeQuery = true)
    List<Authority> findAllRoleAssociatedBy(int userId);
}
