package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Authority {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "authoritymoduleId", nullable = true)
    private Integer authoritymoduleId;
    @Basic
    @Column(name = "code", nullable = true, length = 64)
    private String code;
    @Basic
    @Column(name = "description", nullable = true, length = 128)
    private String description;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;
    @Basic
    @Column(name = "active", nullable = true)
    private Boolean active;
    @Basic
    @Column(name = "deleted", nullable = true)
    private Boolean deleted;
    @Basic
    @Column(name = "observation", nullable = true, length = 64)
    private String observation;
    @OneToMany(mappedBy = "authority")
    private Collection<Roleauthority> roleauthoritiesById;
    @OneToMany(mappedBy = "authority")
    private Collection<Userauthority> userauthoritiesById;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthoritymoduleId() {
        return authoritymoduleId;
    }

    public void setAuthoritymoduleId(Integer authoritymoduleId) {
        this.authoritymoduleId = authoritymoduleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Authority authority = (Authority) o;
        return Objects.equals(id, authority.id) && Objects.equals(authoritymoduleId, authority.authoritymoduleId) && Objects.equals(code, authority.code) && Objects.equals(description, authority.description) && Objects.equals(createdAt, authority.createdAt) && Objects.equals(active, authority.active) && Objects.equals(deleted, authority.deleted) && Objects.equals(observation, authority.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authoritymoduleId, code, description, createdAt, active, deleted, observation);
    }

    public Collection<Roleauthority> getRoleauthoritiesById() {
        return roleauthoritiesById;
    }

    public void setRoleauthoritiesById(Collection<Roleauthority> roleauthoritiesById) {
        this.roleauthoritiesById = roleauthoritiesById;
    }

    public Collection<Userauthority> getUserauthoritiesById() {
        return userauthoritiesById;
    }

    public void setUserauthoritiesById(Collection<Userauthority> userauthoritiesById) {
        this.userauthoritiesById = userauthoritiesById;
    }
}
