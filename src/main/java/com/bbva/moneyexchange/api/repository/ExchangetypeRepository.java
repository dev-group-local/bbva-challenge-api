package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Exchangetype;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ExchangetypeRepository extends CrudRepository<Exchangetype, Integer> {

    Optional<Exchangetype> findFirstByOriginAndTargetAndActiveTrueOrderByIdDesc(String origin, String target);
}
