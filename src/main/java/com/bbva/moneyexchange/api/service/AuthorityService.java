package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Person;
import com.bbva.moneyexchange.api.entity.User;

import java.util.List;

public interface AuthorityService extends GenericService<Authority> {

    List<Authority> findAllRoleAssociatedBy(User user);

    List<Authority> findAllExtraBy(User user);

    List<Authority> findAllBy(User user);

    List<Authority> findAllBy(Person person);

    boolean has(Person person, String authorityCode);
}
