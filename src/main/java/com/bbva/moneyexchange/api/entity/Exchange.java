package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Exchange {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "originCurrency", nullable = true, length = 3)
    private String originCurrency;
    @Basic
    @Column(name = "originAmount", nullable = true, precision = 6)
    private BigDecimal originAmount;
    @Basic
    @Column(name = "targetCurrency", nullable = true, length = 3)
    private String targetCurrency;
    @Basic
    @Column(name = "targetAmount", nullable = true, precision = 6)
    private BigDecimal targetAmount;
    @Basic
    @Column(name = "requestor", nullable = true, length = 32)
    private String requestor;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginCurrency() {
        return originCurrency;
    }

    public void setOriginCurrency(String originCurrency) {
        this.originCurrency = originCurrency;
    }

    public BigDecimal getOriginAmount() {
        return originAmount;
    }

    public void setOriginAmount(BigDecimal originAmount) {
        this.originAmount = originAmount;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exchange exchange = (Exchange) o;
        return Objects.equals(id, exchange.id) && Objects.equals(originCurrency, exchange.originCurrency) && Objects.equals(originAmount, exchange.originAmount) && Objects.equals(targetCurrency, exchange.targetCurrency) && Objects.equals(targetAmount, exchange.targetAmount) && Objects.equals(requestor, exchange.requestor) && Objects.equals(createdAt, exchange.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, originCurrency, originAmount, targetCurrency, targetAmount, requestor, createdAt);
    }
}
