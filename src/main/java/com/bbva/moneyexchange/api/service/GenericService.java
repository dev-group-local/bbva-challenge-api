package com.bbva.moneyexchange.api.service;

public interface GenericService<E> {

    E save(E e);

    void deleteBy(int id);

    void deleteBy(E e);

    E register(E e);

    void edit(E e);
}
