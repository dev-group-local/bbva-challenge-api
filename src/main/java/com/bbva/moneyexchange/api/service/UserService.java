package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface UserService extends GenericService<User> {

    Optional<User> findBy(String username);

    User getBy(String username);

    User getBy(HttpServletRequest request);
}
