package com.bbva.moneyexchange.api.service;

import com.bbva.moneyexchange.api.entity.User;
import com.bbva.moneyexchange.api.entity.Userauthority;

import java.util.List;

public interface UserauthorityService extends GenericService<Userauthority> {

    Userauthority save(Userauthority userauthority);

    List<Userauthority> findAllBy(User user);
}
