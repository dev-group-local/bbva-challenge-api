package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    Optional<User> findByUsernameAndActiveTrueAndDeletedFalse(String username);
}
