package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Exchange;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExchangeRepository extends PagingAndSortingRepository<Exchange, Integer>, JpaSpecificationExecutor<Exchange> {
}
