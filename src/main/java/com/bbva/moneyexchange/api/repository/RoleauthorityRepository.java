package com.bbva.moneyexchange.api.repository;

import com.bbva.moneyexchange.api.entity.Authority;
import com.bbva.moneyexchange.api.entity.Role;
import com.bbva.moneyexchange.api.entity.Roleauthority;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface RoleauthorityRepository extends PagingAndSortingRepository<Roleauthority, Integer>{

    Optional<Roleauthority> findByRoleAndAuthorityAndActiveTrueAndDeletedFalse(Role role, Authority authority);
}
