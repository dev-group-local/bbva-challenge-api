package com.bbva.moneyexchange.api.config;

public class Global {
    public static final Environment MODE = Environment.LOCAL;
    public static final String PROTOCOL;
    public static final String PORT;
    public static final String API_DOMAIN;
    public static final String DOMAIN;
    public static final String ROOT;
    public static final String TYPE;
    public static final String VERSION;
    public static final String ROOT_API_V1;
    public static final String HOST_FRONT;
    public static final String PORT_FRONT;
    public static final String ROOT_FRONT;

    static {
        switch (MODE) {
            case LOCAL:
                DOMAIN = "localhost";
                PROTOCOL = "http";
                PORT = "8085";
                API_DOMAIN = DOMAIN;
                ROOT = PROTOCOL + "://" + API_DOMAIN + ":" + PORT;
                TYPE = "api";
                VERSION = "v1";
                ROOT_API_V1 = PROTOCOL + "://" + API_DOMAIN + ":" + PORT + "/" + TYPE + "/" + VERSION;
                HOST_FRONT = "Not used in local environment";
                PORT_FRONT = "4200";
                ROOT_FRONT = PROTOCOL + "://" + API_DOMAIN + ":" + PORT_FRONT + "/#";
                break;
            default:
                throw new RuntimeException("Undefined environment properties");
        }
    }
}
