package com.bbva.moneyexchange.api.util.allocator;

import com.bbva.moneyexchange.api.entity.Authority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorityAllocator {

    private AuthorityAllocator() {
    }

    public static Collection<GrantedAuthority> toGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream().map(Authority::getCode).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public static void forRegister(Authority authority) {
        ofPostMethod(authority);
    }

    public static void forUpdate(Authority oldAuth, Authority newAuth) {
        oldAuth.setAuthoritymoduleId(newAuth.getAuthoritymoduleId());
        oldAuth.setCode(newAuth.getCode().trim());
        oldAuth.setDescription(newAuth.getDescription().trim());
        oldAuth.setActive(newAuth.getActive());
    }

    public static void forDelete(Authority authority) {
        authority.setDeleted(Boolean.TRUE);
    }

    public static void ofPostMethod(Authority authority) {
        authority.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        authority.setActive(Boolean.TRUE);
        authority.setDeleted(Boolean.FALSE);
    }
}
