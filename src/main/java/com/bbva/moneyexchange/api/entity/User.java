package com.bbva.moneyexchange.api.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class User {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "personId", nullable = false)
    private Integer personId;
    @Basic
    @Column(name = "username", nullable = true, length = 32)
    private String username;
    @Basic
    @Column(name = "password", nullable = true, length = 64)
    private String password;
    @Basic
    @Column(name = "nextSuffixNumber", nullable = true)
    private Integer nextSuffixNumber;
    @Basic
    @Column(name = "credentialsSent", nullable = true)
    private Boolean credentialsSent;
    @Basic
    @Column(name = "credentialsSentAt", nullable = true)
    private Timestamp credentialsSentAt;
    @Basic
    @Column(name = "createdAt", nullable = true)
    private Timestamp createdAt;
    @Basic
    @Column(name = "active", nullable = true)
    private Boolean active;
    @Basic
    @Column(name = "deleted", nullable = true)
    private Boolean deleted;
    @Basic
    @Column(name = "observation", nullable = true, length = 64)
    private String observation;
    @OneToOne
    @JoinColumn(name = "personId", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Person person;
    @OneToMany(mappedBy = "user")
    private Collection<Userauthority> userauthorities;
    @OneToMany(mappedBy = "user")
    private Collection<Userrole> userroles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getNextSuffixNumber() {
        return nextSuffixNumber;
    }

    public void setNextSuffixNumber(Integer nextSuffixNumber) {
        this.nextSuffixNumber = nextSuffixNumber;
    }

    public Boolean getCredentialsSent() {
        return credentialsSent;
    }

    public void setCredentialsSent(Boolean credentialsSent) {
        this.credentialsSent = credentialsSent;
    }

    public Timestamp getCredentialsSentAt() {
        return credentialsSentAt;
    }

    public void setCredentialsSentAt(Timestamp credentialsSentAt) {
        this.credentialsSentAt = credentialsSentAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(personId, user.personId) && Objects.equals(username, user.username) && Objects.equals(password, user.password) && Objects.equals(nextSuffixNumber, user.nextSuffixNumber) && Objects.equals(credentialsSent, user.credentialsSent) && Objects.equals(credentialsSentAt, user.credentialsSentAt) && Objects.equals(createdAt, user.createdAt) && Objects.equals(active, user.active) && Objects.equals(deleted, user.deleted) && Objects.equals(observation, user.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, personId, username, password, nextSuffixNumber, credentialsSent, credentialsSentAt, createdAt, active, deleted, observation);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Collection<Userauthority> getUserauthorities() {
        return userauthorities;
    }

    public void setUserauthorities(Collection<Userauthority> userauthorities) {
        this.userauthorities = userauthorities;
    }

    public Collection<Userrole> getUserroles() {
        return userroles;
    }

    public void setUserroles(Collection<Userrole> userroles) {
        this.userroles = userroles;
    }
}
